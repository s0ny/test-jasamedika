<div class="page-header">
    <h1>
        Search Kelurahan
    </h1>
    <p>
        {{ link_to("kelurahan/new", "Create Kelurahan", "class":"btn btn-primary") }}
    </p>
</div>

{{ content() }}

{{ form("kelurahan/search", "method":"post", "autocomplete" : "off", "class" : "form-horizontal") }}

<div class="form-group">
    <label for="fieldId" class="col-sm-2 control-label">Id</label>
    <div class="col-sm-10">
        {{ text_field("id", "type" : "numeric", "class" : "form-control", "id" : "fieldId") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldNama" class="col-sm-2 control-label">Nama</label>
    <div class="col-sm-10">
        {{ text_field("nama", "size" : 30, "class" : "form-control", "id" : "fieldNama") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldKecamatanId" class="col-sm-2 control-label">Kecamatan</label>
    <div class="col-sm-10">
        {{ select("kecamatan_id", kecamatan, 'using': ['id', 'nama'], "class" : "form-control", "id" : "fieldKecamatanId", 'useEmpty': true) }}
    </div>
</div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {{ submit_button('Search', 'class': 'btn btn-default') }}
    </div>
</div>

</form>
