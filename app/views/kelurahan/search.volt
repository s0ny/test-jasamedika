<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous">{{ link_to("kelurahan/index", "Go Back") }}</li>
            <li class="next">{{ link_to("kelurahan/new", "Create ") }}</li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>Search result</h1>
</div>

{{ content() }}

<div class="row">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Id</th>
            <th>Nama</th>
            <th>Kecamatan</th>

                <th></th>
            </tr>
        </thead>
        <tbody>
        {% if page.items is defined %}
        {% for kelurahan in page.items %}
            <tr>
                <td>{{ kelurahan.id }}</td>
            <td>{{ kelurahan.nama }}</td>
            <td>{{ kelurahan.kecamatan.nama }}</td>

                <td>{{ link_to("kelurahan/edit/"~kelurahan.id, "Edit", "class":"btn btn-default") }} {{ link_to("kelurahan/delete/"~kelurahan.id, "Delete", "class":"btn btn-danger") }}</td>
            </tr>
        {% endfor %}
        {% endif %}
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-sm-1">
        <p class="pagination" style="line-height: 1.42857;padding: 6px 12px;">
            {{ page.current~"/"~page.total_pages }}
        </p>
    </div>
    <div class="col-sm-11">
        <nav>
            <ul class="pagination">
                <li>{{ link_to("kelurahan/search", "First") }}</li>
                <li>{{ link_to("kelurahan/search?page="~page.before, "Previous") }}</li>
                <li>{{ link_to("kelurahan/search?page="~page.next, "Next") }}</li>
                <li>{{ link_to("kelurahan/search?page="~page.last, "Last") }}</li>
            </ul>
        </nav>
    </div>
</div>
