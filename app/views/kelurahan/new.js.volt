$("#fieldKabupatenKotaId").change(function () {
    clearKecamatan();
    var valKabupatenKotaId = $(this).val();
    if (valKabupatenKotaId != "") {
        $.getJSON("{{ url('kelurahan/kecamatan') }}/" + valKabupatenKotaId).done(function (data) {
            var optionKecamatan = [];
            $.each(data, function (index, kecamatan) {
                optionKecamatan.push('<option value="' + kecamatan.id + '">' + kecamatan.nama + '</option>');
            });
            $(optionKecamatan.join("")).appendTo("#fieldKecamatanId");
        });
    }
});

function clearKecamatan() {
    $("#fieldKecamatanId").empty();
}