<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous">{{ link_to("kelurahan", "Go Back") }}</li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>
        Create Kelurahan
    </h1>
</div>

{{ content() }}

{{ form("kelurahan/create", "method":"post", "autocomplete" : "off", "class" : "form-horizontal") }}

<div class="form-group">
    <label for="fieldKabupatenKotaId" class="col-sm-2 control-label">Kabupaten/Kota</label>
    <div class="col-sm-10">
        {{ select("kabupaten_kota_id", kabupatenKota, 'using': ['id', 'nama'], "class" : "form-control", "id" : "fieldKabupatenKotaId", 'useEmpty': true) }}
    </div>
</div>
<div class="form-group">
    <label for="fieldKecamatanId" class="col-sm-2 control-label">Kecamatan</label>
    <div class="col-sm-10">
        {{ select("kecamatan_id", [], "class" : "form-control", "id" : "fieldKecamatanId", "required":"") }}
    </div>
</div>
<div class="form-group">
    <label for="fieldNama" class="col-sm-2 control-label">Nama Kelurahan</label>
    <div class="col-sm-10">
        {{ text_field("nama", "size" : 30, "class" : "form-control", "id" : "fieldNama", "required":"") }}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {{ submit_button('Save', 'class': 'btn btn-primary') }}
    </div>
</div>

</form>

{% do assets.addInlineJs(view.getPartial('kelurahan/new.js')) %}