<div class="page-header">
        <h1>
            Masuk
        </h1>
    </div>
    
    {{ content() }}
    
    {{ form("sessions/logIn", "method":"post", "autocomplete" : "off", "class" : "form-horizontal") }}
    
    <div class="form-group">
        <label for="fieldUsername" class="col-sm-2 control-label">Username</label>
        <div class="col-sm-10">
            {{ text_field("username", "class" : "form-control", "id" : "fieldUsername") }}
        </div>
    </div>
    
    <div class="form-group">
        <label for="fieldPassword" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            {{ password_field("password", "class" : "form-control", "id" : "fieldPassword") }}
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {{ submit_button('Masuk', 'class': 'btn btn-primary') }}
        </div>
    </div>
    
    </form>
    