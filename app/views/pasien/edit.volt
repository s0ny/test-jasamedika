{% do assets.addCss('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css', false) %}
<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous">{{ link_to("pasien", "Go Back") }}</li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>
        Edit Pasien
    </h1>
</div>

{{ content() }}

{{ form("pasien/save", "method":"post", "autocomplete" : "off", "class" : "form-horizontal") }}

<div class="form-group">
    <label for="fieldNama" class="col-sm-2 control-label">Nama</label>
    <div class="col-sm-10">
        {{ text_field("nama", "size" : 30, "class" : "form-control", "id" : "fieldNama") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldAlamat" class="col-sm-2 control-label">Alamat</label>
    <div class="col-sm-10">
        {{ text_area("alamat", "cols": "30", "rows": "4", "class" : "form-control", "id" : "fieldAlamat") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldNoTlp" class="col-sm-2 control-label">No Telepon</label>
    <div class="col-sm-10">
        {{ text_field("no_tlp", "size" : 30, "class" : "form-control", "id" : "fieldNoTlp") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldRt" class="col-sm-2 control-label">RT</label>
    <div class="col-sm-10">
        {{ text_field("rt", "type" : "numeric", "class" : "form-control", "id" : "fieldRt") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldRw" class="col-sm-2 control-label">RW</label>
    <div class="col-sm-10">
        {{ text_field("rw", "type" : "numeric", "class" : "form-control", "id" : "fieldRw") }}
    </div>
</div>

<div class="form-group">
        <label for="fieldKabupatenKotaId" class="col-sm-2 control-label">Kabupaten/Kota</label>
        <div class="col-sm-10">
            {{ select("kabupaten_kota_id", kabupatenKota, 'using': ['id', 'nama'], "class" : "form-control", "id" : "fieldKabupatenKotaId",
            'useEmpty': true) }}
        </div>
    </div>
    
    <div class="form-group">
        <label for="fieldKecamatanId" class="col-sm-2 control-label">Kecamatan</label>
        <div class="col-sm-10">
            {{ select("kecamatan_id", kecamatan, 'using':['id','nama'], "class" : "form-control", "id" : "fieldKecamatanId", 'useEmpty': true, "required":"") }}
        </div>
    </div>
    
    <div class="form-group">
        <label for="fieldKelurahanId" class="col-sm-2 control-label">Kelurahan</label>
        <div class="col-sm-10">
            {{ select("kelurahan_id", kelurahan, 'using':['id','nama'], "class" : "form-control", "id" : "fieldKelurahanId", "required":"") }}
        </div>
    </div>

<div class="form-group">
    <label for="fieldTanggalLahir" class="col-sm-2 control-label">Tanggal Lahir</label>
    <div class="col-sm-10">
        {{ text_field("tanggal_lahir", "type" : "date", "class" : "form-control datepicker", "id" : "fieldTanggalLahir") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldJenisKelamin" class="col-sm-2 control-label">Jenis Kelamin</label>
    <div class="col-sm-10">
        {{ select_static("jenis_kelamin", ['l':'Laki-laki','p':'Perempuan'], "class" : "form-control", "id" : "fieldJenisKelamin") }}
    </div>
</div>


{{ hidden_field("id") }}

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {{ submit_button('Send', 'class': 'btn btn-primary') }}
    </div>
</div>

</form>

{% do assets.addJs('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js', false) %}
{% do assets.addInlineJs(view.getPartial('pasien/new.js')) %}