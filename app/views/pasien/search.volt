<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous">{{ link_to("pasien/index", "Go Back") }}</li>
            <li class="next">{{ link_to("pasien/new", "Register Pasien") }}</li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>Search result</h1>
</div>

{{ content() }}

<div class="row">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Id</th>
            <th>Nama</th>
            <th>Alamat</th>
            <th>No. Telepon</th>
            <th>RT</th>
            <th>RW</th>
            <th>Kelurahan</th>
            <th>Tanggal Lahir</th>
            <th>Jenis Kelamin</th>

                <th></th>
            </tr>
        </thead>
        <tbody>
        {% if page.items is defined %}
        {% for pasien in page.items %}
            <tr>
                <td>{{ pasien.id }}</td>
            <td>{{ pasien.nama }}</td>
            <td>{{ pasien.alamat }}</td>
            <td>{{ pasien.no_tlp }}</td>
            <td>{{ pasien.rt }}</td>
            <td>{{ pasien.rw }}</td>
            <td>{{ pasien.kelurahan.nama }}</td>
            <td>{{ pasien.tanggal_lahir }}</td>
            <td>{{ pasien.jenis_kelamin }}</td>

                <td>{{ link_to("pasien/show/"~pasien.id, "Kartu Pasien", "class": "btn btn-default") }} {{ link_to("pasien/edit/"~pasien.id, "Edit", "class":"btn btn-default") }} {{ link_to("pasien/delete/"~pasien.id, "Delete", "class":"btn btn-danger") }}</td>
            </tr>
        {% endfor %}
        {% endif %}
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-sm-1">
        <p class="pagination" style="line-height: 1.42857;padding: 6px 12px;">
            {{ page.current~"/"~page.total_pages }}
        </p>
    </div>
    <div class="col-sm-11">
        <nav>
            <ul class="pagination">
                <li>{{ link_to("pasien/search", "First") }}</li>
                <li>{{ link_to("pasien/search?page="~page.before, "Previous") }}</li>
                <li>{{ link_to("pasien/search?page="~page.next, "Next") }}</li>
                <li>{{ link_to("pasien/search?page="~page.last, "Last") }}</li>
            </ul>
        </nav>
    </div>
</div>
