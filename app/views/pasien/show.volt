<div class="row">
    <nav>
        <ul class="pager">
            <li class="previous">{{ link_to("pasien", "Go Back") }}</li>
        </ul>
    </nav>
</div>

<div class="page-header">
    <h1>
        Kartu Pasien
    </h1>
</div>

{{ content() }}

<style>
    .data-pasien {
    margin: -200px 20px 12px 50px; 
    position: relative;
}
</style>
<div class="row">
    <div class="col-md-6">
        <img src="/img/kartu-pasien.jpg" class="img-responsive img-thumbnail" alt="Kartu Pasien">
        <div class="data-pasien">
            <h3>{{pasien.id}}</h3>
            <h3>{{pasien.nama}}</h3>
            <h3>{{pasien.tanggal_lahir}}</h3>
        </div>
    </div>
</div>