{% do assets.addCss('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css', false) %}

<div class="page-header">
    <h1>
        Search Pasien
    </h1>
    <p>
        {{ link_to("pasien/new", "Register Pasien", "class":"btn btn-primary") }}
    </p>
</div>

{{ content() }} {{ form("pasien/search", "method":"post", "autocomplete" : "off", "class" : "form-horizontal") }}

<div class="form-group">
    <label for="fieldId" class="col-sm-2 control-label">Id</label>
    <div class="col-sm-10">
        {{ text_field("id", "type" : "numeric", "class" : "form-control", "id" : "fieldId") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldNama" class="col-sm-2 control-label">Nama</label>
    <div class="col-sm-10">
        {{ text_field("nama", "size" : 30, "class" : "form-control", "id" : "fieldNama") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldAlamat" class="col-sm-2 control-label">Alamat</label>
    <div class="col-sm-10">
        {{ text_area("alamat", "cols": "30", "rows": "4", "class" : "form-control", "id" : "fieldAlamat") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldNoTlp" class="col-sm-2 control-label">No Telepon</label>
    <div class="col-sm-10">
        {{ text_field("no_tlp", "size" : 30, "class" : "form-control", "id" : "fieldNoTlp") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldRt" class="col-sm-2 control-label">RT</label>
    <div class="col-sm-10">
        {{ text_field("rt", "type" : "numeric", "class" : "form-control", "id" : "fieldRt") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldRw" class="col-sm-2 control-label">RW</label>
    <div class="col-sm-10">
        {{ text_field("rw", "type" : "numeric", "class" : "form-control", "id" : "fieldRw") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldKelurahanId" class="col-sm-2 control-label">Kelurahan</label>
    <div class="col-sm-10">
        {{ select("kelurahan_id", kelurahan, 'using': ['id', 'nama'], "class" : "form-control", "id" : "fieldKelurahanId", 'useEmpty':
        true) }}
    </div>
</div>

<div class="form-group">
    <label for="fieldTanggalLahir" class="col-sm-2 control-label">Tanggal Lahir</label>
    <div class="col-sm-10">
        {{ text_field("tanggal_lahir", "type" : "date", "class" : "form-control datepicker", "id" : "fieldTanggalLahir") }}
    </div>
</div>

<div class="form-group">
    <label for="fieldJenisKelamin" class="col-sm-2 control-label">Jenis Kelamin</label>
    <div class="col-sm-10">
        {{ select_static("jenis_kelamin", ['l':'Laki-laki','p':'Perempuan'], "class" : "form-control", "id" : "fieldJenisKelamin",
        'useEmpty': true) }}
    </div>
</div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {{ submit_button('Search', 'class': 'btn btn-default') }}
    </div>
</div>

</form>

{% do assets.addJs('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js', false) %}
{% do assets.addInlineJs(view.getPartial('pasien/index.js')) %}