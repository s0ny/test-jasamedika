$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    endDate: 'today'
});

$("#fieldKabupatenKotaId").change(function () {
    clearKecamatan();
    clearKelurahan();

    var valKabupatenKotaId = $(this).val();
    if (valKabupatenKotaId != "") {
        $.getJSON("{{ url('pasien/kecamatan') }}/" + valKabupatenKotaId).done(function (data) {
            var optionKecamatan = [];
            $.each(data, function (index, kecamatan) {
                optionKecamatan.push('<option value="' + kecamatan.id + '">' + kecamatan.nama + '</option>');
            });
            $(optionKecamatan.join("")).appendTo("#fieldKecamatanId");
        });
    }
});

$("#fieldKecamatanId").change(function () {
    clearKelurahan();

    var valKecamatanId = $(this).val();
    if (valKecamatanId != "") {
        $.getJSON("{{ url('pasien/kelurahan') }}/" + valKecamatanId).done(function (data) {
            var optionKelurahan = [];
            $.each(data, function (index, kelurahan) {
                optionKelurahan.push('<option value="' + kelurahan.id + '">' + kelurahan.nama + '</option>');
            });
            $(optionKelurahan.join("")).appendTo("#fieldKelurahanId");
        });
    }
});

function clearKecamatan() {
    $("#fieldKecamatanId").empty();
    $('<option value="">Choose...</option>').appendTo("#fieldKecamatanId");
}

function clearKelurahan() {
    $("#fieldKelurahanId").empty();
}