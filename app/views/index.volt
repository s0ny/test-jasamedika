<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sony Kusyana</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('img/favicon.ico') }}" /> {{ assets.outputCss() }}
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
                    aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Jasamedika</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                {% if session.has('userId') %}
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ url('kelurahan') }}">Data Kelurahan</a>
                    </li>
                    <li>
                        <a href="{{ url('pasien') }}">Data Pasien</a>
                    </li>
                </ul>
                {% endif %}
                <ul class="nav navbar-nav navbar-right">
                        {% if session.has('username') %}
                        <li><a href="#">Hi {{ session.get('username')}},</a></li>
                        {% endif %}
                    <li>
                        {% if session.has('userId') %}
                        <a href="{{ url('sessions/logOut') }}">Keluar</a>
                        {% else %}
                        <a href="{{ url('sessions/signIn') }}">Masuk</a>
                        {% endif %}
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <div class="container">
        {{ content() }}
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> {{ assets.outputJs() }} {{ assets.outputInlineJs() }}
</body>

</html>