<?php
/**
 * Created by PhpStorm.
 * User: sony
 * Date: 03/12/2017
 * Time: 6:59
 */

use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Resource;
use Phalcon\Acl\Role;
use Phalcon\Mvc\User\Component;

class Acl extends Component
{
    private $acl, $filePath;

    public function isAllowed($role, $resource, $action)
    {
        return $this->getAcl()->isAllowed($role, $resource, $this->action($action));
    }

    public function getAcl()
    {
        if (is_object($this->acl)) {
            return $this->acl;
        }

        $filePath = $this->getFilePath();
        if (!file_exists($filePath)) {
            $this->acl = $this->rebuild();
            return $this->acl;
        }

        $data = file_get_contents($filePath);
        $this->acl = unserialize($data);
        return $this->acl;
    }

    protected function getFilePath()
    {
        if (!isset($this->filePath)) {
            $this->filePath = rtrim($this->config->application->cacheDir, '\\/') . '/acl/data.txt';
        }
        return $this->filePath;
    }

    public function rebuild()
    {
        $acl = new AclMemory();

        $acl->setDefaultAction(
            \Phalcon\Acl::DENY
        );

        $acl->addRole(new Role('Admin'));
        $acl->addRole(new Role('Operator'));

        $acl->addResource(new Resource('Kelurahan'), [
            'index',
            'search',
            'new',
            'create',
            'edit',
            'save',
            'delete',
            'kecamatan',
        ]);

        $acl->addResource(new Resource('Pasien'), [
            'index',
            'search',
            'new',
            'create',
            'edit',
            'save',
            'delete',
            'kecamatan',
            'kelurahan',
            'show',
        ]);

        $acl->allow('Admin', 'Kelurahan', 'index');
        $acl->allow('Admin', 'Kelurahan', 'search');
        $acl->allow('Admin', 'Kelurahan', 'new');
        $acl->allow('Admin', 'Kelurahan', 'create');
        $acl->allow('Admin', 'Kelurahan', 'edit');
        $acl->allow('Admin', 'Kelurahan', 'save');
        $acl->allow('Admin', 'Kelurahan', 'delete');
        $acl->allow('Admin', 'Kelurahan', 'kecamatan');
        $acl->allow('Admin', 'Pasien', 'index');
        $acl->allow('Admin', 'Pasien', 'search');
        $acl->allow('Admin', 'Pasien', 'new');
        $acl->allow('Admin', 'Pasien', 'create');
        $acl->allow('Admin', 'Pasien', 'edit');
        $acl->allow('Admin', 'Pasien', 'save');
        $acl->allow('Admin', 'Pasien', 'delete');
        $acl->allow('Admin', 'Pasien', 'kecamatan');
        $acl->allow('Admin', 'Pasien', 'kelurahan');
        $acl->allow('Admin', 'Pasien', 'show');
        
        $acl->allow('Operator', 'Pasien', 'index');
        $acl->allow('Operator', 'Pasien', 'search');
        $acl->allow('Operator', 'Pasien', 'new');
        $acl->allow('Operator', 'Pasien', 'create');
        $acl->allow('Operator', 'Pasien', 'edit');
        $acl->allow('Operator', 'Pasien', 'save');
        $acl->allow('Operator', 'Pasien', 'delete');
        $acl->allow('Operator', 'Pasien', 'kecamatan');
        $acl->allow('Operator', 'Pasien', 'kelurahan');
        $acl->allow('Operator', 'Pasien', 'show');

        $filePath = $this->getFilePath();
        if (touch($filePath) && is_writable($filePath)) {
            file_put_contents($filePath, serialize($acl));
        }

        return $acl;
    }

    private function action($action)
    {
        return !is_null($action) ? $action : 'index';
    }
}
