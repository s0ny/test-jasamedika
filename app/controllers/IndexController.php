<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->session->has('userId')) {
            $this->response->redirect('kelurahan');
        } else {
            $this->response->redirect('sessions/signIn');
        }
    }

    public function ErrorAction()
    {
        
    }
}
