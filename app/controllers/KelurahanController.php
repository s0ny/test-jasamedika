<?php

use Jasamedika\Models\KabupatenKota;
use Jasamedika\Models\Kecamatan;
use Jasamedika\Models\Kelurahan;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class KelurahanController extends ControllerBase
{
    public function beforeExecuteRoute($dispatcher)
    {
        if (!$this->acl->isAllowed($this->session->get('userRole'), 'Kelurahan', $dispatcher->getActionName()))
        {
            $this->flash->warning('Anda tidak mempunyai hak untuk mengakses halaman ini.');

            $this->dispatcher->forward(
                [
                    'controller' => 'index',
                    'action'     => 'error',
                ]
            );

            return false;
        }
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;

        $this->view->kecamatan = Kecamatan::find(['order'=>'nama']);
    }

    /**
     * Searches for kelurahan
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\Jasamedika\Models\Kelurahan', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $kelurahan = Kelurahan::find($parameters);
        if (count($kelurahan) == 0) {
            $this->flash->notice("The search did not find any kelurahan");

            $this->dispatcher->forward([
                "controller" => "kelurahan",
                "action" => "index",
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $kelurahan,
            'limit' => 10,
            'page' => $numberPage,
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        $this->view->kabupatenKota = KabupatenKota::find();
    }

    public function kecamatanAction($kabupatenKotaId)
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            return json_encode(Kecamatan::findByKabupatenKotaId($kabupatenKotaId));
        }
    }

    /**
     * Edits a kelurahan
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $kelurahan = Kelurahan::findFirstByid($id);
            if (!$kelurahan) {
                $this->flash->error("kelurahan was not found");

                $this->dispatcher->forward([
                    'controller' => "kelurahan",
                    'action' => 'index',
                ]);

                return;
            }

            $this->view->id = $kelurahan->id;
            $this->view->kabupatenKota = KabupatenKota::find();
            $this->view->kecamatan = Kecamatan::findByKabupatenKotaId($kelurahan->kecamatan->kabupaten_kota_id);

            $this->tag->setDefault("id", $kelurahan->id);
            $this->tag->setDefault("nama", $kelurahan->nama);
            $this->tag->setDefault("kecamatan_id", $kelurahan->kecamatan_id);
            $this->tag->setDefault("kabupaten_kota_id", $kelurahan->kecamatan->kabupaten_kota_id);

        }
    }

    /**
     * Creates a new kelurahan
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "kelurahan",
                'action' => 'index',
            ]);

            return;
        }

        $kelurahan = new Kelurahan();
        $kelurahan->nama = $this->request->getPost("nama");
        $kelurahan->kecamatan_id = $this->request->getPost("kecamatan_id");

        if (!$kelurahan->save()) {
            foreach ($kelurahan->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "kelurahan",
                'action' => 'new',
            ]);

            return;
        }

        $this->flash->success("kelurahan was created successfully");

        $this->dispatcher->forward([
            'controller' => "kelurahan",
            'action' => 'index',
        ]);
    }

    /**
     * Saves a kelurahan edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "kelurahan",
                'action' => 'index',
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $kelurahan = Kelurahan::findFirstByid($id);

        if (!$kelurahan) {
            $this->flash->error("kelurahan does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "kelurahan",
                'action' => 'index',
            ]);

            return;
        }

        $kelurahan->nama = $this->request->getPost("nama");
        $kelurahan->kecamatan_id = $this->request->getPost("kecamatan_id");

        if (!$kelurahan->save()) {

            foreach ($kelurahan->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "kelurahan",
                'action' => 'edit',
                'params' => [$kelurahan->id],
            ]);

            return;
        }

        $this->flash->success("kelurahan was updated successfully");

        $this->dispatcher->forward([
            'controller' => "kelurahan",
            'action' => 'index',
        ]);
    }

    /**
     * Deletes a kelurahan
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $kelurahan = Kelurahan::findFirstByid($id);
        if (!$kelurahan) {
            $this->flash->error("kelurahan was not found");

            $this->dispatcher->forward([
                'controller' => "kelurahan",
                'action' => 'index',
            ]);

            return;
        }

        if (!$kelurahan->delete()) {

            foreach ($kelurahan->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "kelurahan",
                'action' => 'search',
            ]);

            return;
        }

        $this->flash->success("kelurahan was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "kelurahan",
            'action' => "index",
        ]);
    }

}
