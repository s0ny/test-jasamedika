<?php
use Jasamedika\Models\Users;

class SessionsController extends ControllerBase
{

    public function signInAction()
    {
        if ($this->session->has('userId'))
        {
            $this->response->redirect();
        }
    }

    public function logInAction()
    {
        $user = Users::findFirstByUsername($this->request->getPost('username'));
        if ($user != false)
        {
            if ($user->password === $this->request->getPost('password'))
            {
                $this->session->set('userId', $user->id);
                $this->session->set('username', $user->username);
                $this->session->set('userRole', $user->role);
            }
        }
        else
        {

        }

        $this->response->redirect();
    }

    public function logOutAction()
    {
        $this->session->destroy();
        $this->response->redirect();
    }

}

