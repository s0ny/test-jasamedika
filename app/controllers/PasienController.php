<?php

use Jasamedika\Models\KabupatenKota;
use Jasamedika\Models\Kecamatan;
use Jasamedika\Models\Kelurahan;
use Jasamedika\Models\Pasien;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class PasienController extends ControllerBase
{
    public function beforeExecuteRoute($dispatcher)
    {
        if (!$this->acl->isAllowed($this->session->get('userRole'), 'Pasien', $dispatcher->getActionName()))
        {
            $this->flash->warning('Anda tidak mempunyai hak untuk mengakses halaman ini.');

            $this->dispatcher->forward(
                [
                    'controller' => 'index',
                    'action'     => 'error',
                ]
            );

            return false;
        }
    }

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;

        $this->view->kelurahan = Kelurahan::find(['order'=>'nama']);
    }

    /**
     * Searches for pasien
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\Jasamedika\Models\Pasien', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $pasien = Pasien::find($parameters);
        if (count($pasien) == 0) {
            $this->flash->notice("The search did not find any pasien");

            $this->dispatcher->forward([
                "controller" => "pasien",
                "action" => "index",
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $pasien,
            'limit' => 10,
            'page' => $numberPage,
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {
        $this->view->kabupatenKota = KabupatenKota::find();
    }

    /**
     * Edits a pasien
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $pasien = Pasien::findFirstByid($id);
            if (!$pasien) {
                $this->flash->error("pasien was not found");

                $this->dispatcher->forward([
                    'controller' => "pasien",
                    'action' => 'index',
                ]);

                return;
            }

            $this->view->id = $pasien->id;
            $this->view->kabupatenKota = KabupatenKota::find();
            $this->view->kecamatan = Kecamatan::findByKabupatenKotaId($pasien->kelurahan->kecamatan->kabupaten_kota_id);
            $this->view->kelurahan = Kelurahan::findByKecamatanId($pasien->kelurahan->kecamatan_id);

            $this->tag->setDefault("id", $pasien->id);
            $this->tag->setDefault("nama", $pasien->nama);
            $this->tag->setDefault("alamat", $pasien->alamat);
            $this->tag->setDefault("no_tlp", $pasien->no_tlp);
            $this->tag->setDefault("rt", $pasien->rt);
            $this->tag->setDefault("rw", $pasien->rw);
            $this->tag->setDefault("kelurahan_id", $pasien->kelurahan_id);
            $this->tag->setDefault("kecamatan_id", $pasien->kelurahan->kecamatan_id);
            $this->tag->setDefault("kabupaten_kota_id", $pasien->kelurahan->kecamatan->kabupaten_kota_id);
            $this->tag->setDefault("tanggal_lahir", $pasien->tanggal_lahir);
            $this->tag->setDefault("jenis_kelamin", $pasien->jenis_kelamin);

        }
    }

    private function generateId()
    {
        $prefix = date('ym');

        $id = Pasien::maximum([
            'column' => 'id',
            'conditions' => "id LIKE '" . $prefix . "%'",
        ]);

        if ($id !== null) {
            $newId = (int) substr($id, 4);
            ++$newId;
        } else {
            $newId = 1;
        }

        return $prefix . str_pad($newId, 6, '0', STR_PAD_LEFT);
    }

    /**
     * Creates a new pasien
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "pasien",
                'action' => 'index',
            ]);

            return;
        }

        $pasien = new Pasien();
        $pasien->id = $this->generateId();
        $pasien->nama = $this->request->getPost("nama");
        $pasien->alamat = $this->request->getPost("alamat");
        $pasien->no_tlp = $this->request->getPost("no_tlp");
        $pasien->rt = $this->request->getPost("rt");
        $pasien->rw = $this->request->getPost("rw");
        $pasien->kelurahan_id = $this->request->getPost("kelurahan_id");
        $pasien->tanggal_lahir = $this->request->getPost("tanggal_lahir");
        $pasien->jenis_kelamin = $this->request->getPost("jenis_kelamin");

        if (!$pasien->save()) {
            foreach ($pasien->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "pasien",
                'action' => 'new',
            ]);

            return;
        }

        $this->flash->success("pasien was created successfully");

        $this->dispatcher->forward([
            'controller' => "pasien",
            'action' => 'index',
        ]);
    }

    /**
     * Saves a pasien edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "pasien",
                'action' => 'index',
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $pasien = Pasien::findFirstByid($id);

        if (!$pasien) {
            $this->flash->error("pasien does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "pasien",
                'action' => 'index',
            ]);

            return;
        }

        $pasien->id = $this->request->getPost("id");
        $pasien->nama = $this->request->getPost("nama");
        $pasien->alamat = $this->request->getPost("alamat");
        $pasien->no_tlp = $this->request->getPost("no_tlp");
        $pasien->rt = $this->request->getPost("rt");
        $pasien->rw = $this->request->getPost("rw");
        $pasien->kelurahan_id = $this->request->getPost("kelurahan_id");
        $pasien->tanggal_lahir = $this->request->getPost("tanggal_lahir");
        $pasien->jenis_kelamin = $this->request->getPost("jenis_kelamin");

        if (!$pasien->save()) {

            foreach ($pasien->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "pasien",
                'action' => 'edit',
                'params' => [$pasien->id],
            ]);

            return;
        }

        $this->flash->success("pasien was updated successfully");

        $this->dispatcher->forward([
            'controller' => "pasien",
            'action' => 'index',
        ]);
    }

    /**
     * Deletes a pasien
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $pasien = Pasien::findFirstByid($id);
        if (!$pasien) {
            $this->flash->error("pasien was not found");

            $this->dispatcher->forward([
                'controller' => "pasien",
                'action' => 'index',
            ]);

            return;
        }

        if (!$pasien->delete()) {

            foreach ($pasien->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "pasien",
                'action' => 'search',
            ]);

            return;
        }

        $this->flash->success("pasien was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "pasien",
            'action' => "index",
        ]);
    }

    public function kecamatanAction($kabupatenKotaId)
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            return json_encode(Kecamatan::findByKabupatenKotaId($kabupatenKotaId));
        }
    }

    public function kelurahanAction($kecamatanId)
    {
        if ($this->request->isAjax()) {
            $this->view->disable();
            return json_encode(Kelurahan::findByKecamatanId($kecamatanId));
        }
    }

    public function showAction($id)
    {
        $this->view->pasien = Pasien::findFirstById($id);
    }
}
