<?php

namespace Jasamedika\Models;

class Pasien extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var string
     */
    public $alamat;

    /**
     *
     * @var string
     */
    public $no_tlp;

    /**
     *
     * @var integer
     */
    public $rt;

    /**
     *
     * @var integer
     */
    public $rw;

    /**
     *
     * @var integer
     */
    public $kelurahan_id;

    /**
     *
     * @var string
     */
    public $tanggal_lahir;

    /**
     *
     * @var string
     */
    public $jenis_kelamin;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("test_jasamedika");
        $this->setSource("pasien");
        $this->belongsTo('kelurahan_id', 'Jasamedika\Models\Kelurahan', 'id', ['alias' => 'Kelurahan']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'pasien';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Pasien[]|Pasien|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Pasien|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function beforeSave()
    {
        $this->nama = strtoupper($this->nama);
    }

    public function afterFetch()
    {
        switch ($this->jenis_kelamin) {
            case 'l':
                $this->jenis_kelamin = 'Laki-laki';
                break;
            case 'p':
                $this->jenis_kelamin = 'Perempuan';
                break;
        }
    }
}
