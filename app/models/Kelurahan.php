<?php

namespace Jasamedika\Models;

class Kelurahan extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $nama;

    /**
     *
     * @var integer
     */
    public $kecamatan_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("test_jasamedika");
        $this->setSource("kelurahan");
        $this->belongsTo('kecamatan_id', 'Jasamedika\Models\Kecamatan', 'id', ['alias' => 'Kecamatan']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'kelurahan';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Kelurahan[]|Kelurahan|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Kelurahan|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function beforeSave()
    {
        $this->nama = strtoupper($this->nama);
    }
}
