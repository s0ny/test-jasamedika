# Test Jasamedika

## Screenshoots
### Sign In
![alt text](https://gitlab.com/s0ny/test-jasamedika/raw/7fd8eaaa7a5d0a80fb9024d394dc245a2abef871/screenshoots/signIn.png)
Tampilan form masuk aplikasi, isi username & password "admin" untuk masuk sebagai level Admin dan isi username & password "operator" untuk masuk sebagai level Operator. Untuk lebih lengkapnya silahkan cek tabel users

### Read Data Kelurahan
![alt text](https://gitlab.com/s0ny/test-jasamedika/raw/7fd8eaaa7a5d0a80fb9024d394dc245a2abef871/screenshoots/readKelurahan.png)

### Create Data Kelurahan
![alt text](https://gitlab.com/s0ny/test-jasamedika/raw/7fd8eaaa7a5d0a80fb9024d394dc245a2abef871/screenshoots/createKelurahan.png)

### Update Data Kelurahan
![alt text](https://gitlab.com/s0ny/test-jasamedika/raw/7fd8eaaa7a5d0a80fb9024d394dc245a2abef871/screenshoots/updateKelurahan.png)

### Akses halaman Data Kelurahan sebagai Operator
![alt text](https://gitlab.com/s0ny/test-jasamedika/raw/7fd8eaaa7a5d0a80fb9024d394dc245a2abef871/screenshoots/operatorAksesKelurahan.png)

### Read Data Pasien
![alt text](https://gitlab.com/s0ny/test-jasamedika/raw/7fd8eaaa7a5d0a80fb9024d394dc245a2abef871/screenshoots/readPasien.png)

### Register Data Pasien
![alt text](https://gitlab.com/s0ny/test-jasamedika/raw/7fd8eaaa7a5d0a80fb9024d394dc245a2abef871/screenshoots/createPasien.png)

### Contoh Kartu Pasien setelah register
![alt text](https://gitlab.com/s0ny/test-jasamedika/raw/7fd8eaaa7a5d0a80fb9024d394dc245a2abef871/screenshoots/showPasien.png)


### Prerequisites

```
PHP 7.2.9
Phalcon 3.3.2
MySQL 5.7.22
Composer
```

### Installing

Silahkan install PHP & Phalcon terlebih dahulu pada environment anda, info lengkap installasi framework bisa dilihat [disini](https://docs.phalconphp.com/en/3.3/installation).
Install juga php-devtools, bisa dilihat [disini](https://docs.phalconphp.com/en/3.3/devtools-installation).

Kemudian clone atau download repository ini
```
$ git clone https://gitlab.com/s0ny/test-jasamedika.git
```

Kemudian install composer package yang dibutuhkan
```
$ cd test-jasamedika
$ composer install
```

## Running the tests

Untuk menjalankan unit test:
```
$ cd tests
$ ../vendor/bin/phpunit
```

## Built With

* [PhalconPHP](https://phalconphp.com) - Framework yang digunakan
* [Bootstrap](https://getbootstrap.com) - Tampilan

## Authors

* **Sony Kusyana** - [s0ny](https://gitlab.com/s0ny)