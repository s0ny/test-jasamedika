<?php

namespace Test;

/**
 * Class UnitTest
 */
class PasienTest extends \CntrlrTestCaseF
{

    public function setUp()
    {
        sleep(rand(1, 3));
        parent::setUp();
    }

    /**
     * @dataProvider RouterDataProvider
     * @runInSeparateProcess1
     * @medium
     */

    public function testRouter($c, $a, $p = [], $post = [], $get = [], $session = [], $regext = ['(.*)'], $notRegext = [])
    {
        foreach ($post as $key => $item) {
            $_POST[$key] = $item;
        }

        foreach ($get as $key => $item) {
            $_GET[$key] = $item;
        }

        foreach ($session as $key => $item) {
            $_SESSION[$key] = $item;
        }

        $this->dispatcher->setControllerName($c);
        $this->dispatcher->setActionName($a);
        $this->dispatcher->setParams($p);
        $controller = $this->dispatcher->dispatch();
        $controller->view->start()->render($this->dispatcher->getControllerName(), $this->dispatcher->getActionName(), $controller->view->getParamsToView())->finish();
        $string = $controller->view->getContent();
        foreach ($regext as $regext_i) {
            $this->assertRegExp($regext_i, $string, 'NOT WORK WITH => ' .
                json_encode(
                    ['c' => $c,
                        'a' => $a,
                        'p' => $p,
                        'post' => $post,
                        'get' => $get,
                        'session' => $session,
                        'regext' => $regext]));
        }

        foreach ($notRegext as $regext_i) {
            $this->assertRegExp($regext_i, $string, 'NOT WORK WITH(NEGATIVE) => ' .
                json_encode(
                    ['c' => $c,
                        'a' => $a,
                        'p' => $p,
                        'post' => $post,
                        'get' => $get,
                        'session' => $session,
                        'regext' => $regext,
                        'notRegext' => $notRegext]));
        }

        $_POST = [];
        $_SESSION = [];
        $_GET = [];
    }

    public static function RouterDataProvider()
    {
        return [
            ['c' => 'pasien', 'a' => 'create', 'p' => [], 'post' => ['nama' => 'nama pasien', 'alamat' => 'alamat pasien', 'no_tlp' => '081123456789', 'rt' => '01', 'rw' => '02', 'kelurahan_id' => 1, 'tanggal_lahir' => '2018-08-29', 'jenis_kelamin' => 'p']],
        ];
    }

    public function tearDown()
    {
        parent::tearDown();
    }

}
